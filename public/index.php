<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('ROOT', dirname(__DIR__) . '/');
define('HOST', $_SERVER['SERVER_NAME'] . '/');

require '../vendor/Autoloader.php';

$app = new \Framework\Application();

try {
    echo $app->process();
} catch (Exception $e) {
    echo $e->getMessage();
}