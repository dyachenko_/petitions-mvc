<?php

namespace Controllers;

use Framework\Controller;

class Index extends Controller

{
    public function index()
    {
        return $this->view->render('index/index.phtml');
    }

}
