<?php

namespace Controllers;


use Framework\Controller;

class Error extends Controller
{
    public function index()
    {
        return $this->view->render('error/index.phtml');
    }
}