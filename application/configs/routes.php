<?php

return array(

    '' => 'index/index',
    'index' => 'index/index',
    'error' => 'error/index',
    'login' => 'login/index',
    'login/login' => 'login/login',
    'logout' => 'index/logout',
    'registration' => 'registration/index',
    'petitions/add' => 'petitions/add',
    'petitions/delete' => 'petitions/delete',
    'user' => 'users/index'

);