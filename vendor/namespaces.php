<?php

return array(

    'Framework' => ROOT . "/vendor/Framework/",
    'Controllers' => ROOT . "application/controllers/",
    'Models' => ROOT . "application/models/"

);